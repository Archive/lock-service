/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#include <dbus/dbus.h>
#include <grp.h>
#include <glib.h>

#include "security.h"
#include "globals.h"

#include "connection.h"
#include "watch.h"
#include "user.h"

static GHashTable *lock_service_connections;
static GHashTable *lock_service_users;

static void
drop_user (gpointer data)
{
  User *user = data;

  user_free (user);
}

static void
drop_connection (gpointer data)
{
  Connection *connection = data;
  User *user;

  g_assert (connection_has_no_locks (connection));

  user = connection_get_user_data (connection);

  if (user_decrease_use_count (user))
    {
      gulong uid;

      uid = user_get_uid (user);
      g_hash_table_remove (lock_service_users,
                           (void *) (size_t) uid);
    }

  connection_free (connection);
}

static void
setup_state (void)
{
  lock_service_users = g_hash_table_new_full (NULL, NULL, NULL, drop_user);
  lock_service_connections = g_hash_table_new_full (g_str_hash, g_str_equal,
                                                    g_free, drop_connection);
}

DBusConnection *lock_service_bus;

static gboolean
acquire_bus (void)
{
  DBusError error = DBUS_ERROR_INIT;

  lock_service_bus = dbus_bus_get (DBUS_BUS_SYSTEM, &error);

  if (dbus_error_is_set (&error))
    {
      g_critical ("Can not connect to the system bus");
      g_critical ("%s: %s", error.name, error.message);
      dbus_error_free (&error);

      return FALSE;
    }

  return TRUE;
}

static gboolean
lock_service_get_uid_for_bus_name (const gchar *bus_name,
                                   gulong      *uid)
{
  DBusError error = DBUS_ERROR_INIT;

  *uid = dbus_bus_get_unix_user (lock_service_bus, bus_name, &error);

  if (dbus_error_is_set (&error))
    {
      dbus_error_free (&error);
      return FALSE;
    }

  return TRUE;
}

static void
lock_service_begin (const gchar *bus_name,
                    guint32      reply_serial,
                    const gchar *lock_name,
                    const gchar *object_path,
                    const gchar *message)
{
  Connection *connection;
  gboolean is_owner;
  User *user;

  connection = g_hash_table_lookup (lock_service_connections, bus_name);

  if (connection == NULL)
    {
      User *user;
      gulong uid;

      if (!watch_bus_name (bus_name))
        {
          g_assert_not_reached (); /* XXX */
        }

      if (!lock_service_get_uid_for_bus_name (bus_name, &uid))
        /* the sender disappeared */
        {
          unwatch_bus_name (bus_name);
          return;
        }

      user = g_hash_table_lookup (lock_service_users,
                                  (void *) (size_t) uid);

      if (user == NULL)
        g_hash_table_insert (lock_service_users,
                             (void *) (size_t) uid,
                             user = user_new (uid));

      g_hash_table_insert (lock_service_connections,
                           g_strdup (bus_name),
                           connection = connection_new (user));
      user_increase_use_count (user);
    }

  user = connection_get_user_data (connection);

  is_owner = user_begin (user, bus_name, reply_serial,
                         lock_name, object_path, message);

  if (is_owner)
    connection_add_lock (connection, lock_name);

  else if (connection_has_no_locks (connection))
    g_hash_table_remove (lock_service_connections, bus_name);
}

static void
lock_service_end (const gchar *bus_name,
                  guint32      reply_serial,
                  const gchar *lock_name)
{
  Connection *connection;
  DBusMessage *reply;

  connection = g_hash_table_lookup (lock_service_connections, bus_name);

  if (connection != NULL && connection_remove_lock (connection, lock_name))
    {
      User *user;

      user = connection_get_user_data (connection);
      user_end (user, bus_name, lock_name);

      if (connection_has_no_locks (connection))
        g_hash_table_remove (lock_service_connections, bus_name);

      reply = dbus_message_new (DBUS_MESSAGE_TYPE_METHOD_RETURN);
    }
  else
    {
      reply = dbus_message_new (DBUS_MESSAGE_TYPE_ERROR);
      dbus_message_set_error_name (reply, "ca.desrt.LockService.NotOwner");
      dbus_message_append_args (reply,
                                DBUS_TYPE_STRING,
                                "you do not own the specified lock",
                                DBUS_TYPE_INVALID);
    }

  dbus_message_set_reply_serial (reply, reply_serial);
  dbus_message_set_destination (reply, bus_name);
  dbus_connection_send (lock_service_bus, reply, NULL);
  dbus_message_unref (reply);
}

static void
lock_service_disconnection (const gchar *bus_name)
{
  Connection *connection;
  gchar **locks;
  User *user;
  gint i;

  connection = g_hash_table_lookup (lock_service_connections, bus_name);

  if (connection == NULL)
    return;

  user = connection_get_user_data (connection);
  locks = connection_steal_locks (connection);

  for (i = 0; locks[i]; i++)
    user_end (user, bus_name, locks[i]);

  g_strfreev (locks);

  g_hash_table_remove (lock_service_connections, bus_name);
}

static void
lock_service_introspection_request (const gchar *bus_name,
                                    guint32      reply_serial)
{
  const gchar *introspection_blob = 
    DBUS_INTROSPECT_1_0_XML_DOCTYPE_DECL_NODE
    "<node>\n"
    "  <interface name='org.freedesktop.DBus.Introspectable'>\n"
    "    <method name='Introspect'>\n"
    "      <arg name='data' direction='out' type='s'/>\n"
    "    </method>\n"
    "  </interface>\n"
    "  <interface name='ca.desrt.LockService'>\n"
    "    <method name='Begin'>\n"
    "      <arg name='lock' direction='in' type='s'/>\n"
    "      <arg name='objectpath' direction='in' type='o'/>\n"
    "      <arg name='message' direction='in' type='s'/>\n"
    "      <arg name='isowner' direction='out' type='b'/>\n"
    "      <arg name='result' direction='out' type='s'/>\n"
    "    </method>\n"
    "    <method name='End'>\n"
    "      <arg name='lock' direction='in' type='s'/>\n"
    "    </method>\n"
    "  </interface>\n"
    "</node>\n";
  DBusMessage *reply;

  reply = dbus_message_new (DBUS_MESSAGE_TYPE_METHOD_RETURN);
  dbus_message_set_reply_serial (reply, reply_serial);
  dbus_message_set_destination (reply, bus_name);
  dbus_message_append_args (reply,
                            DBUS_TYPE_STRING, &introspection_blob,
                            DBUS_TYPE_INVALID);
  dbus_connection_send (lock_service_bus, reply, NULL);
  dbus_message_unref (reply);
}

static DBusHandlerResult
lock_service_filter (DBusConnection *connection,
                     DBusMessage    *message,
                     gpointer        user_data)
{
  g_assert (connection == lock_service_bus);
  g_assert (user_data == NULL);

  if (dbus_message_has_path (message, "/"))
    {
      guint32 reply_serial;
      const gchar *sender;

      reply_serial = dbus_message_get_serial (message);
      sender = dbus_message_get_sender (message);     

      if (dbus_message_is_method_call (message,
                                       "org.freedesktop.DBus.Introspectable",
                                       "Introspect"))
        {
          lock_service_introspection_request (sender, reply_serial);

          return DBUS_HANDLER_RESULT_HANDLED;
        }

      else if (dbus_message_is_method_call (message,
                                            "ca.desrt.LockService",
                                            "Begin"))
        {
          const gchar *lock_name, *object_path, *msg;

          if (dbus_message_get_args (message, NULL,
                                     DBUS_TYPE_STRING, &lock_name,
                                     DBUS_TYPE_OBJECT_PATH, &object_path,
                                     DBUS_TYPE_STRING, &msg,
                                     DBUS_TYPE_INVALID))
            {
              lock_service_begin (sender, reply_serial,
                                  lock_name, object_path, msg);

              return DBUS_HANDLER_RESULT_HANDLED;
            }
        }
      else if (dbus_message_is_method_call (message,
                                            "ca.desrt.LockService",
                                            "End"))
        {
          const gchar *lock_name;

          if (dbus_message_get_args (message, NULL,
                                     DBUS_TYPE_STRING, &lock_name,
                                     DBUS_TYPE_INVALID))
            {
              lock_service_end (sender, reply_serial, lock_name);

              return DBUS_HANDLER_RESULT_HANDLED;
            }
        }
    }
  else if (dbus_message_is_signal (message,
                                   "org.freedesktop.DBus",
                                   "NameOwnerChanged") &&
           dbus_message_has_sender (message,
                                    "org.freedesktop.DBus"))
    {
      const gchar *name, *old, *new;

      if (dbus_message_get_args (message, NULL,
                                 DBUS_TYPE_STRING, &name,
                                 DBUS_TYPE_STRING, &old,
                                 DBUS_TYPE_STRING, &new,
                                 DBUS_TYPE_INVALID))
        {
          if (new[0] == '\0')
            lock_service_disconnection (name);
        }
    }

  return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}

static void
setup_filter (void)
{
  dbus_connection_add_filter (lock_service_bus,
                              lock_service_filter,
                              NULL, NULL);
}

static gboolean
acquire_name (const gchar *bus_name)
{
  DBusError error = DBUS_ERROR_INIT;

  if (DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER !=
      dbus_bus_request_name (lock_service_bus, bus_name,
                             DBUS_NAME_FLAG_DO_NOT_QUEUE,
                             &error) ||
      dbus_error_is_set (&error))
    {
      g_critical ("Could not acquire '%s'", bus_name);

      if (dbus_error_is_set (&error))
        {
          g_critical ("%s: %s", error.name, error.message);
          dbus_error_free (&error);
        }

      return FALSE;
    }

  return TRUE;
}

int
main (void)
{
  security_initialise ();

  remove_effective_privs ();
  assert_no_effective_privs ();

  if (!acquire_bus () || !acquire_name ("ca.desrt.LockService"))
    return 1;

  restore_effective_privs ();
  set_root_directory ();
  remove_all_privs ();
  assert_no_privs ();

  setup_filter ();
  setup_state ();
  
  while (dbus_connection_read_write_dispatch (lock_service_bus, -1));

  return 0;
}
