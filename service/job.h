/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#ifndef _job_h_
#define _job_h_

#include <glib.h>

typedef struct OPAQUE_TYPE__Job                             Job;

typedef void          (*JobDoneNotify)                                  (Job            *job,
                                                                         gpointer        user_data);
                               
void                    job_send                                        (Job            *job,
                                                                         const gchar    *bus_name,
                                                                         const gchar    *object_path);

void                    job_return_to_origin                            (Job            *job,
                                                                         gchar         **origin_bus_name,
                                                                         gchar         **origin_object_path);

Job *                   job_new                                         (const gchar    *bus_name,
                                                                         guint32         reply_serial,
                                                                         const gchar    *object_path,
                                                                         const gchar    *message,
                                                                         JobDoneNotify   notify,
                                                                         gpointer        user_data);

#endif /* _job_h_ */
