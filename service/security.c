/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#include "security.h"

#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <glib.h>
#include <grp.h>
#include <pwd.h>

static gchar *target_dir;
static uid_t target_uid;
static gid_t target_gid;

void
security_initialise (void)
{
  struct passwd *entry;

  if (getuid () != 0)
    g_error ("lock-service must be run as root");

  errno = 0;
  entry = getpwnam (LOCK_SERVICE_USER);

  if (entry == NULL)
    {
      if (errno != 0)
        g_error ("getpwnam ('%s'): %s\n",
                 LOCK_SERVICE_USER, g_strerror (errno));
      else
        g_error ("getpwnam ('%s'): No such user\n", LOCK_SERVICE_USER);
    }

  target_dir = g_strdup (entry->pw_dir);
  target_uid = entry->pw_uid;
  target_gid = entry->pw_gid;
}

void
remove_effective_privs (void)
{
  setgroups (0, NULL);
  setgid (target_gid);
  seteuid (target_uid);
}

void
restore_effective_privs (void)
{
  seteuid (0);
  setuid (0);

  g_assert (geteuid () == 0);
  g_assert (getuid () == 0);
}

void
remove_all_privs (void)
{
  setgid (target_gid);
  setuid (target_uid);
}

void
assert_no_effective_privs (void)
{
  gint n;

  n = getgroups (0, NULL);
  g_assert (n == 0 || n == 1);
  if (n == 1)
    {
      gid_t group;

      n = getgroups (1, &group);
      g_assert (n == 1);

      g_assert (group == target_gid);
    }

  g_assert (getgid () == target_gid);
  g_assert (getegid () == target_gid);
  g_assert (geteuid () == target_uid);
}

void
assert_no_privs (void)
{
  assert_no_effective_privs ();

  g_assert (getuid () == target_uid);
}

void
set_root_directory (void)
{
  if (g_mkdir_with_parents (target_dir, 0700))
    g_error ("g_mkdir_with_parents ('%s'): %s\n",
              target_dir, g_strerror (errno));

  if (chown (target_dir, target_uid, target_gid))
    g_error ("chown ('%s', uid, gid): %s\n",
              target_dir, g_strerror (errno));

  if (chroot (target_dir))
    g_error ("chroot ('%s'): %s\n",
             target_dir, g_strerror (errno));

  if (chdir ("/"))
    g_error ("chroot ('/'): %s\n", g_strerror (errno));
}
