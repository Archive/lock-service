/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#include <dbus/dbus.h>

#include "globals.h"
#include "job.h"

struct OPAQUE_TYPE__Job
{
  DBusPendingCall *pending;
  guint32 reply_serial;

  gchar *bus_name;
  gchar *object_path;
  gchar *data;

  JobDoneNotify notify;
  gpointer user_data;
};

static void
job_drop (Job *job)
{
  job->notify (job, job->user_data);

  g_free (job->bus_name);
  g_free (job->object_path);
  g_free (job->data);

  g_slice_free (Job, job);
}

static void
job_cancel_pending (Job *job)
{
  if (job->pending != NULL)
    {
      dbus_pending_call_cancel (job->pending);
      dbus_pending_call_unref (job->pending);
      job->pending = NULL;
    }
}

static DBusMessage *
job_construct_reply (Job  *job,
                     gint  type)
{
  DBusMessage *message;

  message = dbus_message_new (type);
  dbus_message_set_destination (message, job->bus_name);
  dbus_message_set_reply_serial (message, job->reply_serial);

  return message;
}


static void
job_done (DBusPendingCall *pending,
          gpointer         user_data)
{
  Job *job = user_data;
  DBusMessage *result;
  DBusMessage *reply;

  g_assert (job->pending == pending);
  reply = dbus_pending_call_steal_reply (pending);

  if (dbus_message_get_type (reply) == DBUS_MESSAGE_TYPE_METHOD_RETURN &&
      dbus_message_has_signature (reply, "s"))
    {
      dbus_bool_t f = FALSE;
      const gchar *data;

      result = job_construct_reply (job, DBUS_MESSAGE_TYPE_METHOD_RETURN);

      dbus_message_get_args (reply, NULL,
                             DBUS_TYPE_STRING, &data,
                             DBUS_TYPE_INVALID);

      dbus_message_append_args (result,
                                DBUS_TYPE_BOOLEAN, &f,
                                DBUS_TYPE_STRING, &data,
                                DBUS_TYPE_INVALID);
    }
  else
    {
      result = job_construct_reply (job, DBUS_MESSAGE_TYPE_ERROR);
      dbus_message_set_error_name (result, "ca.desrt.lock.wtf");
    }

  dbus_connection_send (lock_service_bus, result, NULL);
  dbus_message_unref (result);
  dbus_message_unref (reply);

  job_drop (job);
}

void
job_send (Job         *job,
          const gchar *bus_name,
          const gchar *object_path)
{
  DBusMessage *message;

  job_cancel_pending (job);

  message = dbus_message_new_method_call (bus_name, object_path,
                                          "ca.desrt.LockService.Client",
                                          "Request");
  dbus_message_append_args (message,
                            DBUS_TYPE_STRING, &job->data,
                            DBUS_TYPE_INVALID);

  dbus_connection_send_with_reply (lock_service_bus,
                                   message, &job->pending, -1);
  dbus_pending_call_set_notify (job->pending, job_done, job, NULL);
  dbus_message_unref (message);
}

void
job_return_to_origin (Job    *job,
                      gchar **origin_connection,
                      gchar **origin_object_path)
{
  dbus_bool_t t = TRUE;
  DBusMessage *reply;

  job_cancel_pending (job);

  reply = job_construct_reply (job, DBUS_MESSAGE_TYPE_METHOD_RETURN);
  dbus_message_append_args (reply,
                            DBUS_TYPE_BOOLEAN, &t,
                            DBUS_TYPE_STRING, &job->data,
                            DBUS_TYPE_INVALID);
  dbus_connection_send (lock_service_bus, reply, NULL);
  dbus_message_unref (reply);

  *origin_connection = g_strdup (job->bus_name);
  *origin_object_path = g_strdup (job->object_path);

  job_drop (job);
}

Job *
job_new (const gchar   *bus_name,
         guint32        reply_serial,
         const gchar   *object_path,
         const gchar   *data,
         JobDoneNotify  notify,
         gpointer       user_data)
{
  Job *job;

  job = g_slice_new (Job);

  job->bus_name = g_strdup (bus_name);
  job->object_path = g_strdup (object_path);
  job->data = g_strdup (data);

  job->reply_serial = reply_serial;
  job->pending = NULL;

  job->notify = notify;
  job->user_data = user_data;

  return job;
}
