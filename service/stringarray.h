/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#ifndef _stringarray_h_
#define _stringarray_h_

#include <glib.h>

void                    string_array_append                             (gchar       ***array,
                                                                         gint          *length,
                                                                         const gchar   *string);

gboolean                string_array_remove                             (gchar       ***array,
                                                                         gint          *length,
                                                                         const gchar   *string);

void                    string_array_shift                              (gchar       ***array,
                                                                         gint          *length,
                                                                         gint           n);

gchar **                string_array_steal                              (gchar       ***array,
                                                                         gint          *length);


#endif /* _stringarray_h_ */
