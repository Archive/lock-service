/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#include "lock.h"

#include "job.h"

struct OPAQUE_TYPE__Lock
{
  gchar *owner_bus_name;
  gchar *owner_object_path;

  Job **jobs;
  int n_jobs;
};

static void
lock_add_job (Lock *lock,
              Job  *job)
{
  lock->jobs = g_renew (Job *, lock->jobs, lock->n_jobs + 1);
  lock->jobs[lock->n_jobs++] = job;
}

static void
lock_remove_job (Job      *job,
                 gpointer  user_data)
{
  Lock *lock = user_data;
  gint i;

  lock->n_jobs--;

  for (i = 0; lock->jobs[i] != job; i++)
    g_assert_cmpint (i, <, lock->n_jobs);

  for (; i < lock->n_jobs; i++)
    lock->jobs[i] = lock->jobs[i + 1];

  lock->jobs = g_renew (Job *, lock->jobs, lock->n_jobs);
}

gboolean
lock_begin (Lock        *lock,
            const gchar *bus_name,
            guint32      reply_serial,
            const gchar *object_path,
            const gchar *message)
{
  gboolean is_owner;
  Job *job;

  job = job_new (bus_name, reply_serial, object_path,
                 message, lock_remove_job, lock);
  lock_add_job (lock, job);

  is_owner = lock->owner_bus_name == NULL;

  if (is_owner)
    job_return_to_origin (job,
                          &lock->owner_bus_name,
                          &lock->owner_object_path);

  else
    job_send (job,
              lock->owner_bus_name,
              lock->owner_object_path);

  return is_owner;
}

gboolean
lock_end (Lock        *lock,
          const gchar *bus_name)
{
  gint i;

  g_assert_cmpstr (bus_name, ==, lock->owner_bus_name);

  g_free (lock->owner_bus_name);
  g_free (lock->owner_object_path);

  lock->owner_bus_name = NULL;
  lock->owner_object_path = NULL;

  if (lock->n_jobs > 0)
    job_return_to_origin (lock->jobs[0],
                          &lock->owner_bus_name,
                          &lock->owner_object_path);

  for (i = 0; i < lock->n_jobs; i++)
    job_send (lock->jobs[i],
              lock->owner_bus_name,
              lock->owner_object_path);

  return lock->owner_bus_name == NULL;
}

void
lock_free (Lock *lock)
{
  g_assert (lock->owner_bus_name == NULL);
  g_assert (lock->owner_object_path == NULL);
  g_assert (lock->n_jobs == 0);
  g_free (lock->jobs);

  g_slice_free (Lock, lock);
}

Lock *
lock_new (void)
{
  return g_slice_new0 (Lock);
}
