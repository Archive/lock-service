/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#include "connection.h"

#include "stringarray.h"

struct OPAQUE_TYPE__Connection
{
  gpointer user_data;

  gchar **locks;
  gint n_locks;
};

Connection *
connection_new (gpointer user_data)
{
  Connection *connection;

  connection = g_slice_new (Connection);
  connection->user_data = user_data;
  connection->locks = NULL;
  connection->n_locks = 0;

  return connection;
}

void
connection_free (Connection *connection)
{
  g_assert (connection_has_no_locks (connection));
  g_free (connection->locks);

  g_slice_free (Connection, connection);
}

gpointer
connection_get_user_data (Connection *connection)
{
  return connection->user_data;
}

gboolean
connection_has_no_locks (Connection *connection)
{
  return connection->n_locks == 0;
}

void
connection_add_lock (Connection  *connection,
                     const gchar *lock_name)
{
  string_array_append (&connection->locks,
                       &connection->n_locks,
                       lock_name);
}

gboolean
connection_remove_lock (Connection  *connection,
                        const gchar *lock_name)
{
  return string_array_remove (&connection->locks,
                              &connection->n_locks,
                              lock_name);
}

gchar **
connection_steal_locks (Connection *connection)
{
  return string_array_steal (&connection->locks,
                             &connection->n_locks);
}
