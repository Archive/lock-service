/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#ifndef _watch_h_
#define _watch_h_

#include <glib.h>

gboolean                watch_bus_name                                  (const gchar *bus_name);
void                    unwatch_bus_name                                (const gchar *bus_name);

#endif /* _watch_h_ */
