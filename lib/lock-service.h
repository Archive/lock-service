/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#ifndef _lock_service_h_
#define _lock_service_h_

#include <gio/gio.h>

G_BEGIN_DECLS

#define LOCK_SERVICE_ERROR                                  lock_service_error_quark ()

typedef enum
{
  LOCK_SERVICE_ERROR_FAILED
} LockServiceError;

GQuark                  lock_service_error_quark                        (void);

#define LOCK_SERVICE_TYPE_LOCK                               lock_service_lock_get_type ()
#define LOCK_SERVICE_LOCK(inst)                             (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                             LOCK_SERVICE_TYPE_LOCK, LockServiceLock))
#define LOCK_SERVICE_LOCK_CLASS(class)                      (G_TYPE_CHECK_CLASS_CAST ((class),                       \
                                                             LOCK_SERVICE_TYPE_LOCK, LockServiceLockClass))
#define LOCK_SERVICE_IS_LOCK(inst)                          (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                             LOCK_SERVICE_TYPE_LOCK))
#define LOCK_SERVICE_IS_LOCK_CLASS(class)                   (G_TYPE_CHECK_CLASS_TYPE ((class),                       \
                                                             LOCK_SERVICE_TYPE_LOCK))
#define LOCK_SERVICE_LOCK_GET_CLASS(inst)                   (G_TYPE_INSTANCE_GET_CLASS ((inst),                      \
                                                             LOCK_SERVICE_TYPE_LOCK, LockServiceLockClass))

typedef struct _LockServiceLockPrivate                      LockServiceLockPrivate;
typedef struct _LockServiceLockClass                        LockServiceLockClass;
typedef struct _LockServiceLock                             LockServiceLock;

struct _LockServiceLockClass
{
  GObjectClass parent_class;

  gchar * (*request)        (LockServiceLock     *lock,
                             const gchar         *request);
  void    (*request_async)  (LockServiceLock     *lock,
                             const gchar         *request,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data);
  gchar * (*request_finish) (LockServiceLock     *lock,
                             GAsyncResult        *result);
};

struct _LockServiceLock
{
  GObject parent_instance;

  /*< private >*/
  LockServiceLockPrivate *priv;
};

GType                   lock_service_lock_get_type                      (void);

gboolean                lock_service_lock_begin                         (LockServiceLock      *lock,
                                                                         const gchar          *request,
                                                                         gchar               **reply,
                                                                         GError              **error);

void                    lock_service_lock_begin_async                   (LockServiceLock      *lock,
                                                                         const gchar          *request,
                                                                         GAsyncReadyCallback   callback,
                                                                         gpointer              user_data);

gboolean                lock_service_lock_begin_finish                  (LockServiceLock      *lock,
                                                                         GAsyncResult         *result,
                                                                         gchar               **reply,
                                                                         GError              **error);

void                    lock_service_lock_end                           (LockServiceLock      *lock);

gchar *                 lock_service_lock_request                       (LockServiceLock      *lock,
                                                                         const gchar          *request);
void                    lock_service_lock_request_async                 (LockServiceLock      *lock,
                                                                         const gchar          *request,
                                                                         GAsyncReadyCallback   callback,
                                                                         gpointer              user_data);
gchar *                 lock_service_lock_request_finish                (LockServiceLock      *lock,
                                                                         GAsyncResult         *result);

LockServiceLock *       lock_service_lock_new                           (const gchar          *name);
LockServiceLock *       lock_service_lock_construct                     (GType                 type,
                                                                         const gchar          *name);

G_END_DECLS

#endif /* _lock_service_h_ */
