/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#ifndef _lock_h_
#define _lock_h_

#include <glib.h>

typedef struct OPAQUE_TYPE__Lock                            Lock;

void                    lock_free                                       (Lock        *lock);
Lock *                  lock_new                                        (void);

gboolean                lock_begin                                      (Lock        *lock,
                                                                         const gchar *bus_name,
                                                                         guint32      reply_serial,
                                                                         const gchar *object_path,
                                                                         const gchar *message);

gboolean                lock_end                                        (Lock        *lock,
                                                                         const gchar *bus_name);

#endif /* _lock_h_ */
