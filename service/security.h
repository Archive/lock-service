/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#ifndef _security_h_
#define _security_h_

void                    remove_effective_privs                          (void);
void                    restore_effective_privs                         (void);
void                    remove_all_privs                                (void);
void                    assert_no_effective_privs                       (void);
void                    assert_no_privs                                 (void);
void                    set_root_directory                              (void);
void                    security_initialise                             (void);

#endif /* _security_h_ */
