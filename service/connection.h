/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#ifndef _connection_h_
#define _connection_h_

#include <dbus/dbus.h>
#include <glib.h>

typedef struct OPAQUE_TYPE__Connection                      Connection;

void                    connection_free                                 (Connection  *connection);
Connection *            connection_new                                  (gpointer     user_data);
gpointer                connection_get_user_data                        (Connection  *connection);

gchar **                connection_steal_locks                          (Connection  *connection);

void                    connection_add_lock                             (Connection  *connection,
                                                                         const gchar *lock_name);

gboolean                connection_remove_lock                          (Connection  *connection,
                                                                         const gchar *lock_name);

gboolean                connection_has_no_locks                         (Connection  *connection);

gboolean                connection_has_lock                             (Connection  *connection,
                                                                         const gchar *lock_name);

#endif /* _connection_h_ */
