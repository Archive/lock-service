/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#include "lock-service.h"

#include <dbus/dbus.h>

/**
 * LockServiceError:
 * @LOCK_SERVICE_ERROR_FAILED: a generic error.  Currently, all errors
 *                             are generic errors.
 *
 * Error codes for problems that can occur with #LockServiceLock
 * operations.
 **/

/**
 * LOCK_SERVICE_ERROR:
 *
 * Error domain for #LockServiceLock operations.  Errors in this domain
 * will be from the #LockServiceError enumeration.  See #GError for
 * information on error domains.
 **/
GQuark
lock_service_error_quark (void)
{
  static GQuark quark;

  if G_UNLIKELY (quark == 0)
    quark = g_quark_from_string ("lock-service-error-quark");

  return quark;
}

static gboolean lock_service_setup (GError **error);
static DBusConnection *lock_service_connection;
static GHashTable *lock_service_locks;

G_DEFINE_TYPE (LockServiceLock, lock_service_lock, G_TYPE_OBJECT)

/**
 * SECTION:lock
 * @Short_Description: a GObject representing a lock-service lock
 *
 * 
 **/

/**
 * LockServiceLock:
 *
 * This is an opaque structure; it may not be accessed directly.
 **/

/**
 * LockServiceLockClass:
 * @request: virtual function pointer for lock_service_lock_request().
 *           The default implementation ignores the request and returns
 *           the empty string.  Overriding the default implementation of
 *           @request_async means that this function will never be
 *           called unless you explicitly call it.
 * @request_async: virtual function pointer for
 *                 lock_service_lock_request_async().  The default
 *                 implementation of this function calls the
 *                 non-asynchronous version, puts the result into a
 *                 #GSimpleAsyncResult and dispatches it in an idle
 *                 handler on the default mainloop.
 * @request_finish: virtual function pointer for
 *                  lock_service_lock_request_finish().  The default
 *                  implementation of this function expects a
 *                  #GSimpleAsyncResult with the @source_tag set to
 *                  lock_service_lock_request_async() and the payload
 *                  set to a weak pointer to the reply string (ie: the
 *                  string will be duplicated before it is returned).
 *
 * The class structure for #LockServiceLock.
 **/

struct _LockServiceLockPrivate
{
  gchar *name;

  gpointer pending;
  gboolean is_owner;
};

static DBusMessage *
lock_service_begin_message (const gchar *lock_name,
                            const gchar *request)
{
  DBusMessage *message;
  gboolean ok;

  message = dbus_message_new_method_call ("ca.desrt.LockService",
                                          "/",
                                          "ca.desrt.LockService",
                                          "Begin");
  g_assert (message != NULL);

  ok = dbus_message_append_args (message,
                                 DBUS_TYPE_STRING, &lock_name,
                                 DBUS_TYPE_OBJECT_PATH, &lock_name,
                                 DBUS_TYPE_STRING, &request,
                                 DBUS_TYPE_INVALID);
  g_assert (ok);

  return message;
}

static gboolean
lock_service_convert_error (GError    **error,
                            DBusError  *d_error)
{
  if (d_error->message)
    g_set_error (error, LOCK_SERVICE_ERROR, LOCK_SERVICE_ERROR_FAILED,
                 "%s: %s", d_error->name, d_error->message);
  else
    g_set_error (error, LOCK_SERVICE_ERROR, LOCK_SERVICE_ERROR_FAILED,
                 "dbus error: %s", d_error->name);

  dbus_error_free (d_error);

  return FALSE;
}

static void
lock_service_lock_get_property (GObject *object, guint prop_id,
                                GValue *value, GParamSpec *pspec)
{
  LockServiceLock *lock = LOCK_SERVICE_LOCK (object);

  g_assert (prop_id == 1);
  g_value_set_string (value, lock->priv->name);
}

static void
lock_service_lock_set_property (GObject *object, guint prop_id,
                                const GValue *value, GParamSpec *pspec)
{
  LockServiceLock *lock = LOCK_SERVICE_LOCK (object);

  g_assert (prop_id == 1);
  g_assert (lock->priv->name == NULL);
  lock->priv->name = g_value_dup_string (value);
}

static gchar *
lock_service_lock_real_request (LockServiceLock *lock,
                                const gchar     *request)
{
  return g_strdup ("");
}

static gchar *
lock_service_lock_real_request_finish (LockServiceLock *lock,
                                       GAsyncResult    *result)
{
  GSimpleAsyncResult *simple;

  g_return_val_if_fail (
    g_simple_async_result_is_valid (result, G_OBJECT (lock),
      lock_service_lock_request_async), NULL);

  simple = G_SIMPLE_ASYNC_RESULT (result);

  return g_strdup (g_simple_async_result_get_op_res_gpointer (simple));
}

static void
lock_service_lock_real_request_async (LockServiceLock     *lock,
                                      const gchar         *request,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  GSimpleAsyncResult *simple;

  simple = g_simple_async_result_new (G_OBJECT (lock), callback, user_data,
                                      lock_service_lock_request_async);
  g_simple_async_result_set_op_res_gpointer (simple, g_strdup (""), g_free);
  g_simple_async_result_complete_in_idle (simple);
  g_object_unref (simple);
}

static void
lock_service_lock_finalize (GObject *object)
{
  LockServiceLock *lock = LOCK_SERVICE_LOCK (object);

  g_free (lock->priv->name);

  G_OBJECT_CLASS (lock_service_lock_parent_class)
    ->finalize (object);
}

static void
lock_service_lock_init (LockServiceLock *lock)
{
  lock->priv = G_TYPE_INSTANCE_GET_PRIVATE (lock,
                                            LOCK_SERVICE_TYPE_LOCK,
                                            LockServiceLockPrivate);
}

static void
lock_service_lock_class_init (LockServiceLockClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  object_class->set_property = lock_service_lock_set_property;
  object_class->get_property = lock_service_lock_get_property;
  object_class->finalize = lock_service_lock_finalize;
  class->request = lock_service_lock_real_request;
  class->request_async = lock_service_lock_real_request_async;
  class->request_finish = lock_service_lock_real_request_finish;

  g_object_class_install_property (object_class, 1,
    g_param_spec_string ("name", "lock name",
                         "the unique name of the lock to acquire", NULL,
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE |
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (class, sizeof (LockServiceLockPrivate));
}

/**
 * lock_service_lock_begin:
 * @lock: a #LockServiceLock
 * @request: the request string, non-%NULL
 * @reply: a pointer to the reply from the service, or %NULL
 * @error: a pointer to a %NULL #GError, or %NULL
 *
 * Acquire the lock or send a message to the existing owner.
 *
 * The lock is acquired if and only if a lock of the same name is not
 * being held by the same user on the same system.
 *
 * It is an error to call this function if @lock is already holding the
 * lock.
 *
 * In the event that the lock is acquired (ie: we now hold the lock),
 * @reply will be set to %NULL, %TRUE will be returned and the reference
 * count of @lock will be increased by one.  The reference count will
 * drop again when lock_service_lock_end() is called.
 *
 * In the event that the lock is already held then @request (which must
 * be non-%NULL) will be sent to the holder.  The holder's reply (which
 * will also be non-%NULL) is placed in @reply and %TRUE is returned.
 * The reference count will not be increased.
 *
 * In the event of an error, @error will be set accordingly and %FALSE
 * returned.
 *
 * While the lock is held, @lock may receive requests.  These requests
 * result in calls being made to lock_service_lock_request_async().  For
 * non-trivial uses, you will want to override this virtual function.
 * See #LockServiceLockClass.
 *
 * lock_service_lock_request() is not called for the @request given to
 * this function.  If you want that, then check for @reply being set to
 * %NULL, then invoke the function yourself.
 **/
gboolean
lock_service_lock_begin (LockServiceLock  *lock,
                         const gchar      *request,
                         gchar           **reply,
                         GError          **error)
{
  DBusError d_error = DBUS_ERROR_INIT;
  DBusMessage *message, *result;
  const gchar *result_text;
  gboolean is_owner;

  g_return_val_if_fail (LOCK_SERVICE_IS_LOCK (lock), FALSE);
  g_return_val_if_fail (request != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);
  g_return_val_if_fail (lock->priv->is_owner == FALSE, FALSE);
  g_return_val_if_fail (lock->priv->pending == FALSE, FALSE);

  if (!lock_service_setup (error))
    return FALSE;

  message = lock_service_begin_message (lock->priv->name, request);
  result = dbus_connection_send_with_reply_and_block (lock_service_connection,
                                                      message, -1, &d_error);
  dbus_message_unref (message);

  if (result == NULL)
    return lock_service_convert_error (error, &d_error);

  if (!dbus_message_get_args (result, &d_error,
                              DBUS_TYPE_BOOLEAN, &is_owner,
                              DBUS_TYPE_STRING, &result_text,
                              DBUS_TYPE_INVALID))
    {
      dbus_message_unref (result);

      return lock_service_convert_error (error, &d_error);
    }

  if (is_owner)
    {
      g_hash_table_insert (lock_service_locks,
                           lock->priv->name,
                           g_object_ref (lock));
      lock->priv->is_owner = TRUE;
    }

  if (reply != NULL)
    {
      if (is_owner)
        *reply = NULL;
      else
        *reply = g_strdup (result_text);
    }

  return TRUE;
}

/**
 * lock_service_lock_begin_async:
 * @lock: a #LockServiceLock
 * @request: the request string, non-%NULL
 * @callback: a #GAsyncReadyCallback
 * @user_data: user data for @callback
 *
 * This is the first half of the asynchronous variant of
 * lock_service_lock_begin().  The result of this function should be
 * collected by calling lock_service_lock_begin_finish() from @callback.
 *
 * No other calls may be made on @lock while the operation is in
 * progress.
 **/
void
lock_service_lock_begin_async (LockServiceLock     *lock,
                               const gchar         *request,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  g_return_if_fail (LOCK_SERVICE_IS_LOCK (lock));
  g_return_if_fail (lock->priv->pending == NULL);
  g_return_if_fail (lock->priv->is_owner);
  g_return_if_fail (request != NULL);

  lock->priv->pending = lock_service_lock_begin_async;

  {
    GError *error = NULL;

    if (!lock_service_setup (&error))
      {
        g_simple_async_report_gerror_in_idle (G_OBJECT (lock),
                                              callback, user_data,
                                              error);
        g_error_free (error);

        return;
      }
  }

  g_assert_not_reached ();
}

/**
 * lock_service_lock_begin_finish:
 * @lock: the #LockServiceLock passed to the ready callback
 * @result: the #GAsyncResult passed to the ready callback
 * @reply: a pointer to the reply from the service, or %NULL
 * @error: a pointer to a %NULL #GError, or %NULL
 *
 * This is the second half of the asynchronous variant of
 * lock_service_lock_begin().  This function should be called from the
 * callback given to lock_service_lock_begin_async().
 **/
gboolean
lock_service_lock_begin_finish (LockServiceLock  *lock,
                                GAsyncResult     *result,
                                gchar           **reply,
                                GError          **error)
{
  GSimpleAsyncResult *simple;

  g_return_val_if_fail (LOCK_SERVICE_IS_LOCK (lock), FALSE);
  g_return_val_if_fail (G_IS_SIMPLE_ASYNC_RESULT (result), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  simple = G_SIMPLE_ASYNC_RESULT (result);

  if (g_simple_async_result_propagate_error (simple, error))
    return FALSE;

  if (reply != NULL)
    *reply = g_strdup (g_simple_async_result_get_op_res_gpointer (simple));

  return TRUE;
}

/**
 * lock_service_lock_end:
 * @lock: a #LockServiceLock
 *
 * Releases the lock.  The reference count of @lock is decreased.
 *
 * It is an error to call this function if @lock is not holding the
 * lock.
 **/
void
lock_service_lock_end (LockServiceLock *lock)
{
  DBusMessage *message;
  gboolean ok;

  g_return_if_fail (LOCK_SERVICE_IS_LOCK (lock));
  g_return_if_fail (lock->priv->is_owner);

  message = dbus_message_new_method_call ("ca.desrt.LockService", "/",
                                          "ca.desrt.LockService", "End");
  g_assert (message != NULL);

  dbus_message_set_no_reply (message, TRUE);

  ok = dbus_message_append_args (message,
                                 DBUS_TYPE_STRING, &lock->priv->name,
                                 DBUS_TYPE_INVALID);
  g_assert (ok);

  ok = dbus_connection_send (lock_service_connection, message, NULL);
  g_assert (ok);

  dbus_message_unref (message);

  g_hash_table_remove (lock_service_locks, lock->priv->name);
}

/**
 * lock_service_lock_request:
 * @lock: a #LockServiceLock
 * @request: the request string, non-%NULL
 * @returns: a newly-allocated string
 *
 * Makes a request to @lock.
 *
 * The exact nature of what this call does depends on how the virtual
 * functions in the #LockServiceLockClass structure are overridden.
 *
 * It is appropriate to call g_free() on the return value.
 **/
gchar *
lock_service_lock_request (LockServiceLock *lock,
                           const gchar     *request)
{
  gchar *reply;

  g_return_val_if_fail (LOCK_SERVICE_IS_LOCK (lock), NULL);
  g_return_val_if_fail (lock->priv->pending == FALSE, NULL);
  g_return_val_if_fail (lock->priv->is_owner, NULL);
  g_return_val_if_fail (request != NULL, NULL);

  lock->priv->pending = lock_service_lock_request;

  reply = LOCK_SERVICE_LOCK_GET_CLASS (lock)
    ->request (lock, request);

  lock->priv->pending = NULL;

  return NULL;
}

/**
 * lock_service_lock_request_async:
 * @lock: a #LockServiceLock
 * @request: the request string, non-%NULL
 * @callback: a #GAsyncReadyCallback
 * @user_data: user data for @callback
 *
 * This is the first half of the asynchronous variant of
 * lock_service_lock_request().  The result of this function should be
 * collected by calling lock_service_lock_request_finish() from
 * @callback.
 *
 * No other calls may be made on @lock while the operation is in
 * progress.
 **/
void
lock_service_lock_request_async (LockServiceLock     *lock,
                                 const gchar         *request,
                                 GAsyncReadyCallback  callback,
                                 gpointer             user_data)
{
  g_return_if_fail (LOCK_SERVICE_IS_LOCK (lock));
  g_return_if_fail (lock->priv->pending == NULL);
  g_return_if_fail (lock->priv->is_owner);
  g_return_if_fail (request != NULL);

  lock->priv->pending = lock_service_lock_request_async;

  LOCK_SERVICE_LOCK_GET_CLASS (lock)
    ->request_async (lock, request, callback, user_data);

  /* request_async implementation *must* return result to the mainloop */
  g_assert (lock->priv->pending != NULL);
}

/**
 * lock_service_lock_request_finish:
 * @lock: a #LockServiceLock
 * @result: the #GAsyncResult passed to the ready callback
 *
 * This is the second half of the asynchronous variant of
 * lock_service_lock_request().  This function should be called from the
 * callback given to lock_service_lock_request_async().
 **/
gchar *
lock_service_lock_request_finish (LockServiceLock *lock,
                                  GAsyncResult    *result)
{
  gchar *reply;

  g_return_val_if_fail (LOCK_SERVICE_IS_LOCK (lock), NULL);
  g_return_val_if_fail (lock->priv->pending ==
                        lock_service_lock_request_async, NULL);
  g_return_val_if_fail (lock->priv->is_owner, NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);

  reply = LOCK_SERVICE_LOCK_GET_CLASS (lock)
    ->request_finish (lock, result);

  lock->priv->pending = NULL;

  return reply;
}

/**
 * lock_service_lock_construct:
 * @type: %LOCK_SERVICE_TYPE_LOCK, or a subclass #GType
 * @name: the name of the lock
 * @returns: a new #LockServiceLock, or subclass
 *
 * Constructs a #LockServiceLock, or subclass.
 *
 * This is the Dova-style (ie: Vala-friendly) construction function.
 * See lock_service_lock_new() for normal uses.
 **/
LockServiceLock *
lock_service_lock_construct (GType        type,
                             const gchar *name)
{
  return g_object_new (type, "name", name, NULL);
}

/**
 * lock_service_lock_new:
 * @name: the name of the lock
 * @returns: a new #LockServiceLock
 *
 * Creates a new #LockServiceLock with the unique name of @name.
 *
 * A lock of a given name can only be held once by a given user on a
 * given machine.  Two users on the same machine can hold locks of the
 * same name.  A user may hold locks of two different names.  A user may
 * hold a lock of the same name on two different machines.
 **/
LockServiceLock *
lock_service_lock_new (const gchar *name)
{
  return lock_service_lock_construct (LOCK_SERVICE_TYPE_LOCK, name);
}

static void
lock_service_request_done (GObject      *object,
                           GAsyncResult *result,
                           gpointer      user_data)
{
  LockServiceLock *lock = LOCK_SERVICE_LOCK (object);
  DBusMessage *message = user_data;
  gchar *reply;
  gboolean ok;

  reply = lock_service_lock_request_finish (lock, result);
  g_assert (reply != NULL);

  ok = dbus_message_append_args (message,
                                 DBUS_TYPE_STRING, &reply,
                                 DBUS_TYPE_INVALID);
  g_assert (ok);
  g_free (reply);

  ok = dbus_connection_send (lock_service_connection, message, NULL);
  g_assert (ok);

  dbus_message_unref (message);
}

static DBusHandlerResult
lock_service_filter (DBusConnection *connection,
                     DBusMessage    *message,
                     gpointer        user_data)
{
  if (dbus_message_is_method_call (message,
                                   "ca.desrt.LockService.Client",
                                   "Request"))
    {
      const gchar *request, *path;

      path = dbus_message_get_path (message);

      if (dbus_message_get_args (message, NULL,
                                 DBUS_TYPE_STRING, &request,
                                 DBUS_TYPE_INVALID))
        {
          LockServiceLock *lock;

          lock = g_hash_table_lookup (lock_service_locks, path);

          if (lock != NULL)
            {
              DBusMessage *result;

              result = dbus_message_new_method_return (message);
              g_assert (result != NULL);

              lock_service_lock_request_async (lock, request,
                                               lock_service_request_done,
                                               result);

              return DBUS_HANDLER_RESULT_HANDLED;
            }
          else
            {
              DBusMessage *error;
              gboolean ok;

              error = dbus_message_new_error (message,
                                              "ca.desrt.LockService.Client.NoSuchLock",
                                              "the specified lock does not exist");
              g_assert (error != NULL);

              ok = dbus_connection_send (connection, error, NULL);
              g_assert (ok);

              dbus_message_unref (error);

              return DBUS_HANDLER_RESULT_HANDLED;
            }
        }
    }

  return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}

static void _g_dbus_connection_integrate_with_main (DBusConnection *);

static gboolean
lock_service_setup (GError **error)
{
  static gboolean initialised;

  if (!initialised)
    {
      DBusError d_error = DBUS_ERROR_INIT;
      gboolean ok;

      lock_service_locks = g_hash_table_new_full (g_str_hash, g_str_equal,      
                                                  NULL, g_object_unref);

      lock_service_connection = dbus_bus_get (DBUS_BUS_SYSTEM, &d_error);

      if (lock_service_connection == NULL)
        return lock_service_convert_error (error, &d_error);

      ok = dbus_connection_add_filter (lock_service_connection,
                                       lock_service_filter,
                                       NULL, NULL);
      g_assert (ok);

      _g_dbus_connection_integrate_with_main (lock_service_connection);

      initialised = TRUE;
    }

  return TRUE;
}

/* ------------------------------------------------------------------------ */
/* all code past this point is for mainloop integration.
 *
 * this code was lifted from common/gdbusutils.c in gvfs.
 * it has been slightly modified to remove its dependence on libgio.
 *
 * Copyright (C) 2006-2007 Red Hat, Inc.
 * Author: Alexander Larsson <alexl@redhat.com>
 * Modified: Ryan Lortie <desrt@desrt.ca>
 */

typedef gboolean (*GFDSourceFunc) (gpointer data,
                                   GIOCondition condition,
                                   int fd);

static void
_g_dbus_oom (void)
{
  g_error ("DBus failed with out of memory error");
}

/*************************************************************************
 *             Helper fd source                                          *
 ************************************************************************/

typedef struct
{
  GSource source;
  GPollFD pollfd;
} FDSource;

static gboolean
fd_source_prepare (GSource  *source,
		   gint     *timeout)
{
  *timeout = -1;

  return FALSE;
}

static gboolean
fd_source_check (GSource  *source)
{
  FDSource *fd_source = (FDSource *)source;

  return fd_source->pollfd.revents != 0;
}

static gboolean
fd_source_dispatch (GSource     *source,
		    GSourceFunc  callback,
		    gpointer     user_data)

{
  GFDSourceFunc func = (GFDSourceFunc)callback;
  FDSource *fd_source = (FDSource *)source;

  g_assert (func != NULL);

  return (*func) (user_data, fd_source->pollfd.revents, fd_source->pollfd.fd);
}

static GSourceFuncs fd_source_funcs = {
  fd_source_prepare,
  fd_source_check,
  fd_source_dispatch,
  NULL
};

/* Two __ to avoid conflict with gio version */
static GSource *
__g_fd_source_new (int fd,
		   gushort events)
{
  GSource *source;
  FDSource *fd_source;

  source = g_source_new (&fd_source_funcs, sizeof (FDSource));
  fd_source = (FDSource *)source;

  fd_source->pollfd.fd = fd;
  fd_source->pollfd.events = events;
  g_source_add_poll (source, &fd_source->pollfd);

  return source;
}

/*************************************************************************
 *                                                                       *
 *      dbus mainloop integration for async ops                          *
 *                                                                       *
 *************************************************************************/

static gint32 main_integration_data_slot = -1;
static GOnce once_init_main_integration = G_ONCE_INIT;

/*
 * A GSource subclass for dispatching DBusConnection messages.
 * We need this on top of the IO handlers, because sometimes
 * there are messages to dispatch queued up but no IO pending.
 *
 * The source is owned by the connection (and the main context
 * while that is alive)
 */
typedef struct
{
  GSource source;

  DBusConnection *connection;
  GSList *ios;
  GSList *timeouts;
} DBusSource;

typedef struct
{
  DBusSource *dbus_source;
  GSource *source;
  DBusWatch *watch;
} IOHandler;

typedef struct
{
  DBusSource *dbus_source;
  GSource *source;
  DBusTimeout *timeout;
} TimeoutHandler;

static gpointer
main_integration_init (gpointer arg)
{
  if (!dbus_connection_allocate_data_slot (&main_integration_data_slot))
    g_error ("Unable to allocate data slot");

  return NULL;
}

static gboolean
dbus_source_prepare (GSource *source,
		     gint    *timeout)
{
  DBusConnection *connection = ((DBusSource *)source)->connection;

  *timeout = -1;

  return (dbus_connection_get_dispatch_status (connection) == DBUS_DISPATCH_DATA_REMAINS);
}

static gboolean
dbus_source_check (GSource *source)
{
  return FALSE;
}

static gboolean
dbus_source_dispatch (GSource     *source,
		      GSourceFunc  callback,
		      gpointer     user_data)
{
  DBusConnection *connection = ((DBusSource *)source)->connection;

  dbus_connection_ref (connection);

  /* Only dispatch once - we don't want to starve other GSource */
  dbus_connection_dispatch (connection);

  dbus_connection_unref (connection);

  return TRUE;
}

static gboolean
io_handler_dispatch (gpointer data,
                     GIOCondition condition,
                     int fd)
{
  IOHandler *handler = data;
  guint dbus_condition = 0;
  DBusConnection *connection;

  connection = handler->dbus_source->connection;

  if (connection)
    dbus_connection_ref (connection);

  if (condition & G_IO_IN)
    dbus_condition |= DBUS_WATCH_READABLE;
  if (condition & G_IO_OUT)
    dbus_condition |= DBUS_WATCH_WRITABLE;
  if (condition & G_IO_ERR)
    dbus_condition |= DBUS_WATCH_ERROR;
  if (condition & G_IO_HUP)
    dbus_condition |= DBUS_WATCH_HANGUP;

  /* Note that we don't touch the handler after this, because
   * dbus may have disabled the watch and thus killed the
   * handler.
   */
  dbus_watch_handle (handler->watch, dbus_condition);
  handler = NULL;

  if (connection)
    dbus_connection_unref (connection);

  return TRUE;
}

static void
io_handler_free (IOHandler *handler)
{
  DBusSource *dbus_source;

  dbus_source = handler->dbus_source;
  dbus_source->ios = g_slist_remove (dbus_source->ios, handler);

  g_source_destroy (handler->source);
  g_source_unref (handler->source);
  g_free (handler);
}

static void
dbus_source_add_watch (DBusSource *dbus_source,
		       DBusWatch *watch)
{
  guint flags;
  GIOCondition condition;
  IOHandler *handler;
  int fd;

  if (!dbus_watch_get_enabled (watch))
    return;

  g_assert (dbus_watch_get_data (watch) == NULL);

  flags = dbus_watch_get_flags (watch);

  condition = G_IO_ERR | G_IO_HUP;
  if (flags & DBUS_WATCH_READABLE)
    condition |= G_IO_IN;
  if (flags & DBUS_WATCH_WRITABLE)
    condition |= G_IO_OUT;

  handler = g_new0 (IOHandler, 1);
  handler->dbus_source = dbus_source;
  handler->watch = watch;

#if (DBUS_MAJOR_VERSION == 1 && DBUS_MINOR_VERSION == 1 && DBUS_MICRO_VERSION >= 1) || (DBUS_MAJOR_VERSION == 1 && DBUS_MINOR_VERSION > 1) || (DBUS_MAJOR_VERSION > 1)
  fd = dbus_watch_get_unix_fd (watch);
#else
  fd = dbus_watch_get_fd (watch);
#endif

  handler->source = __g_fd_source_new (fd, condition);
  g_source_set_callback (handler->source,
			 (GSourceFunc) io_handler_dispatch, handler,
                         NULL);
  g_source_attach (handler->source, NULL);

  dbus_source->ios = g_slist_prepend (dbus_source->ios, handler);
  dbus_watch_set_data (watch, handler,
		       (DBusFreeFunction)io_handler_free);
}

static void
dbus_source_remove_watch (DBusSource *dbus_source,
			  DBusWatch *watch)
{
  dbus_watch_set_data (watch, NULL, NULL);
}

static void
timeout_handler_free (TimeoutHandler *handler)
{
  DBusSource *dbus_source;

  dbus_source = handler->dbus_source;
  dbus_source->timeouts = g_slist_remove (dbus_source->timeouts, handler);

  g_source_destroy (handler->source);
  g_source_unref (handler->source);
  g_free (handler);
}

static gboolean
timeout_handler_dispatch (gpointer      data)
{
  TimeoutHandler *handler = data;

  dbus_timeout_handle (handler->timeout);

  return TRUE;
}

static void
dbus_source_add_timeout (DBusSource *dbus_source,
			 DBusTimeout *timeout)
{
  TimeoutHandler *handler;

  if (!dbus_timeout_get_enabled (timeout))
    return;

  g_assert (dbus_timeout_get_data (timeout) == NULL);

  handler = g_new0 (TimeoutHandler, 1);
  handler->dbus_source = dbus_source;
  handler->timeout = timeout;

  handler->source = g_timeout_source_new (dbus_timeout_get_interval (timeout));
  g_source_set_callback (handler->source,
			 timeout_handler_dispatch, handler,
                         NULL);
  g_source_attach (handler->source, NULL);

  /* handler->source is owned by the context here */
  dbus_source->timeouts = g_slist_prepend (dbus_source->timeouts, handler);

  dbus_timeout_set_data (timeout, handler,
			 (DBusFreeFunction)timeout_handler_free);
}

static void
dbus_source_remove_timeout (DBusSource *source,
			    DBusTimeout *timeout)
{
  dbus_timeout_set_data (timeout, NULL, NULL);
}

static dbus_bool_t
add_watch (DBusWatch *watch,
	   gpointer   data)
{
  DBusSource *dbus_source = data;

  dbus_source_add_watch (dbus_source, watch);

  return TRUE;
}

static void
remove_watch (DBusWatch *watch,
	      gpointer   data)
{
  DBusSource *dbus_source = data;

  dbus_source_remove_watch (dbus_source, watch);
}

static void
watch_toggled (DBusWatch *watch,
               void      *data)
{
  /* Because we just exit on OOM, enable/disable is
   * no different from add/remove */
  if (dbus_watch_get_enabled (watch))
    add_watch (watch, data);
  else
    remove_watch (watch, data);
}

static dbus_bool_t
add_timeout (DBusTimeout *timeout,
	     void        *data)
{
  DBusSource *source = data;

  if (!dbus_timeout_get_enabled (timeout))
    return TRUE;

  dbus_source_add_timeout (source, timeout);

  return TRUE;
}

static void
remove_timeout (DBusTimeout *timeout,
		void        *data)
{
  DBusSource *source = data;

  dbus_source_remove_timeout (source, timeout);
}

static void
timeout_toggled (DBusTimeout *timeout,
                 void        *data)
{
  /* Because we just exit on OOM, enable/disable is
   * no different from add/remove
   */
  if (dbus_timeout_get_enabled (timeout))
    add_timeout (timeout, data);
  else
    remove_timeout (timeout, data);
}

static void
wakeup_main (void *data)
{
  g_main_context_wakeup (NULL);
}

static const GSourceFuncs dbus_source_funcs = {
  dbus_source_prepare,
  dbus_source_check,
  dbus_source_dispatch
};

/* Called when the connection dies or when we're unintegrating from mainloop */
static void
dbus_source_free (DBusSource *dbus_source)
{
  while (dbus_source->ios)
    {
      IOHandler *handler = dbus_source->ios->data;

      dbus_watch_set_data (handler->watch, NULL, NULL);
    }

  while (dbus_source->timeouts)
    {
      TimeoutHandler *handler = dbus_source->timeouts->data;

      dbus_timeout_set_data (handler->timeout, NULL, NULL);
    }

  /* Remove from mainloop */
  g_source_destroy ((GSource *)dbus_source);

  g_source_unref ((GSource *)dbus_source);
}

static void
_g_dbus_connection_remove_from_main (DBusConnection *connection)
{
  g_once (&once_init_main_integration, main_integration_init, NULL);

  if (!dbus_connection_set_data (connection,
				 main_integration_data_slot,
				 NULL, NULL))
    _g_dbus_oom ();
}

static void
_g_dbus_connection_integrate_with_main (DBusConnection *connection)
{
  DBusSource *dbus_source;

  g_once (&once_init_main_integration, main_integration_init, NULL);

  g_assert (connection != NULL);

  _g_dbus_connection_remove_from_main (connection);

  dbus_source = (DBusSource *)
    g_source_new ((GSourceFuncs*)&dbus_source_funcs,
		  sizeof (DBusSource));

  dbus_source->connection = connection;

  if (!dbus_connection_set_watch_functions (connection,
                                            add_watch,
                                            remove_watch,
                                            watch_toggled,
                                            dbus_source, NULL))
    _g_dbus_oom ();

  if (!dbus_connection_set_timeout_functions (connection,
                                              add_timeout,
                                              remove_timeout,
                                              timeout_toggled,
                                              dbus_source, NULL))
    _g_dbus_oom ();

  dbus_connection_set_wakeup_main_function (connection,
					    wakeup_main,
					    dbus_source, NULL);

  /* Owned by both connection and mainloop (until destroy) */
  g_source_attach ((GSource *)dbus_source, NULL);

  if (!dbus_connection_set_data (connection,
				 main_integration_data_slot,
				 dbus_source, (DBusFreeFunction)dbus_source_free))
    _g_dbus_oom ();
}
