/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#include "watch.h"

#include <dbus/dbus.h>

#include "stringarray.h"
#include "globals.h"

static gboolean watching_all;
static gchar **extra_watches;
static gint n_extra_watches;
static gint n_watches;

static gboolean
match_req (void        (*dbus_func) (DBusConnection *connection,
                                     const gchar    *rule,
                                     DBusError      *error),
           const gchar  *bus_name)
{
  DBusError error = DBUS_ERROR_INIT;

  if (bus_name != NULL)
    {
      gchar *rule;

      rule = g_strdup_printf ("sender='org.freedesktop.DBus',"
                              "member='NameOwnerChanged',"
                              "arg0='%s'", bus_name);
      dbus_func (lock_service_bus, rule, &error);
      g_free (rule);
    }
  else
    dbus_func (lock_service_bus,
               "sender='org.freedesktop.DBus',"
               "member='NameOwnerChanged'",
               &error);

  if (dbus_error_is_set (&error))
    {
      dbus_error_free (&error);
      return FALSE;
    }

  return TRUE;
}

gboolean
watch_bus_name (const gchar *bus_name)
{
  if (n_watches < 100)
    {
      if (!match_req (dbus_bus_add_match, bus_name))
        return FALSE;

      n_watches++;
    }
  else
    {
      if (!watching_all)
        {
          if (!match_req (dbus_bus_add_match, NULL))
            return FALSE;

          watching_all = TRUE;
        }

      string_array_append (&extra_watches, &n_extra_watches, bus_name);
    }

  return TRUE;
}

void
unwatch_bus_name (const gchar *bus_name)
{
  if (watching_all)
    {
      if (string_array_remove (&extra_watches, &n_extra_watches, bus_name))
        {
          if (n_watches + n_extra_watches * 2 < 50)
            {
              gint i;

              for (i = 0; i < n_extra_watches; i++)
                {
                  if (!match_req (dbus_bus_add_match, extra_watches[i]))
                    break;

                  n_watches++;
                }

              string_array_shift (&extra_watches, &n_extra_watches, i);

              if (n_extra_watches == 0)
                {
                  if (!match_req (dbus_bus_remove_match, NULL))
                    return;

                  watching_all = FALSE;
                }
            }
        }
    }
  else
    {
      if (!match_req (dbus_bus_remove_match, bus_name))
        return;

      n_watches--;
    }
}
