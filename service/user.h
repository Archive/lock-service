/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#ifndef _user_h_
#define _user_h_

#include <glib.h>

typedef struct OPAQUE_TYPE__User                            User;

void                    user_increase_use_count                         (User        *user);
gboolean                user_decrease_use_count                         (User        *user);
void                    user_free                                       (User        *user);
User *                  user_new                                        (gulong       uid);
gulong                  user_get_uid                                    (User        *user);

gboolean                user_begin                                      (User        *user,
                                                                         const gchar *bus_name,
                                                                         guint32      reply_serial,
                                                                         const gchar *lock_name,
                                                                         const gchar *object_path,
                                                                         const gchar *message);

void                    user_end                                        (User        *user,
                                                                         const gchar *bus_name,
                                                                         const gchar *lock_name);

#endif /* _user_h_ */
