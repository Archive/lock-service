/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#include "user.h"

#include <dbus/dbus.h>
#include "globals.h"
#include "lock.h"

struct OPAQUE_TYPE__User
{
  GHashTable *locks;

  unsigned long uid;
  gint refs;
};

void
user_free (User *user)
{
  g_assert_cmpint (g_hash_table_size (user->locks), ==, 0);
  g_assert_cmpint (user->refs, ==, 0);

  g_slice_free (User, user);
}

gulong
user_get_uid (User *user)
{
  return user->uid;
}

void
user_increase_use_count (User *user)
{
  g_assert_cmpint (user->refs, >=, 0);

  user->refs++;
}

gboolean
user_decrease_use_count (User *user)
{
  g_assert_cmpint (user->refs, >, 0);

  return 0 == --user->refs;
}

static void
lock_free_wrapper (gpointer user_data)
{
  lock_free (user_data);
}

User *
user_new (gulong uid)
{
  User *user;

  user = g_slice_new (User);
  user->locks = g_hash_table_new_full (g_str_hash, g_str_equal,
                                       g_free, lock_free_wrapper);
  user->uid = uid;
  user->refs = 0;

  return user;
}

gboolean
user_begin (User        *user,
            const gchar *bus_name,
            guint32      reply_serial,
            const gchar *lock_name,
            const gchar *object_path,
            const gchar *message)
{
  Lock *lock;

  lock = g_hash_table_lookup (user->locks, lock_name);

  if (lock == NULL)
    g_hash_table_insert (user->locks,
                         g_strdup (lock_name),
                         lock = lock_new ());

  return lock_begin (lock, bus_name, reply_serial, object_path, message);
}

void
user_end (User *user,
          const gchar *bus_name,
          const gchar *lock_name)
{
  gboolean is_unowned;
  Lock *lock;

  lock = g_hash_table_lookup (user->locks, lock_name);
  g_assert (lock != NULL);

  is_unowned = lock_end (lock, bus_name);

  if (is_unowned)
    g_hash_table_remove (user->locks, lock_name);
}
