/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

#include "stringarray.h"

#include <string.h>

void
string_array_append (gchar       ***array,
                     gint          *length,
                     const gchar   *string)
{
  *array = g_renew (gchar *, *array, *length + 1);
  (*array)[(*length)++] = g_strdup (string);
}

gboolean
string_array_remove (gchar       ***array,
                     gint          *length,
                     const gchar   *string)
{
  gint i;

  for (i = 0; i < *length; i++)
    if (strcmp ((*array)[i], string) == 0)
      {
        (*length)--;
        memmove (*array + i, *array + i + 1, *length - i);
        *array = g_renew (gchar *, *array, *length);

        return TRUE;
      }

  return FALSE;
}

void
string_array_shift (gchar ***array,
                    gint    *length,
                    gint     n)
{
  gint i;

  g_assert_cmpint (n, <=, *length);

  for (i = 0; i < n; i++)
    g_free ((*array)[i]);

  *length -= n;
  memmove (*array, *array + n, *length);
  *array = g_renew (gchar *, *array, *length);
}

gchar **
string_array_steal (gchar ***array,
                    gint    *length)
{
  gchar **result;

  string_array_append (array, length, NULL);
  result = *array;
  *array = NULL;
  *length = 0;

  return result;
}
