/*
 * Copyright © 2009 Codethink Limited
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the licence, or (at your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 * Authors: Ryan Lortie <desrt@desrt.ca>
 */

[CCode (cheader_filename = "lock-service.h")]
namespace LockService {
  public class Lock : GLib.Object {
    public string name { construct; get; }

    virtual async string request_async (string request);
    virtual string request (string request);

    public Lock (string name);

    public async bool begin_async (string request, out string? reply) throws GLib.Error;
    public bool begin (string request, out string? reply) throws GLib.Error;
    public void end ();
  }
}
